export type CreateSlsHttpProxyProps = {
  tokens: string | string[] | Set<string> | ((token: string) => boolean)
  headerKeys?: {
    token?: string
    host?: string
    ip?: string
  }
  dropHeaderKeys?: string | string[] | Set<string> | ((key: string) => boolean)
  message?: string
}

export type SlsHttpProxy = {
  (request: Request): Promise<Response>
}

type CheckFn<T> = (val: T) => boolean

const createCheckFn = <T>(
  value: T | T[] | Set<T> | CheckFn<T> | undefined | null
): CheckFn<T> => {
  if (typeof value === 'function') return value as CheckFn<T>
  if (typeof value === 'boolean') return (val: T) => value
  if (Array.isArray(value) || value) {
    const _value = new Set([value].flat())
    return (target: T) => _value.has(target)
  }
  return () => false
}

export const createSlsHttpProxy = ({
  tokens,
  headerKeys,
  dropHeaderKeys = (key) => key.startsWith('cf-'),
  message = 'Welcome to nginx!',
}: CreateSlsHttpProxyProps): SlsHttpProxy => {
  const {
    token: TOKEN_HEADER = 'Px-Token',
    host: HOST_HEADER = 'Px-Host',
    ip: IP_HEADER = 'Px-IP'
  } = headerKeys ?? {}

  const _checkTokenAllowed = createCheckFn(tokens)

  const _checkHeaderDrop = createCheckFn(dropHeaderKeys)

  return async (request: Request): Promise<Response> => {
    const host = request.headers.get(HOST_HEADER) ?? ''
    const token = request.headers.get(TOKEN_HEADER) ?? ''
    const ip = request.headers.get(IP_HEADER) ?? ''

    if (!_checkTokenAllowed(token)) {
      return new Response(message)
    }

    const url = new URL(request.url)
    url.host = host

    const req = new Request(url.href, request)

    req.headers.set('Host', host)
    req.headers.set('X-Forwarded-For', ip)

    req.headers.delete(TOKEN_HEADER.toLowerCase())
    req.headers.delete(HOST_HEADER.toLowerCase())
    req.headers.delete(IP_HEADER.toLowerCase())
    req.headers.delete('x-real-ip')

    for (const key of req.headers.keys()) {
      if (_checkHeaderDrop(key)) {
        req.headers.delete(key)
      }
    }

    return fetch(req)
  }
}
