# sls-http-proxy
Serverless Http Proxy on Cloudflare Worker, Deno Deploy or more ...

# Get Start

## Deploy to Serverless platform
Choose your favorite platform to deploy some code, check the simple code on below.

### Cloudflare Worker
```mjs
import { createSlsHttpProxy } from 'sls-http-proxy'

// get tokens from env `TOKENS` and split with `,`
const tokens = TOKENS.split(',')

const proxy = createSlsHttpProxy({ tokens })

addEventListener('fetch', event => {
  event.respondWith(proxy(event.request))
})
```

### Deno Deploy
```ts
import { serve } from 'https://deno.land/std@0.114.0/http/server.ts'
import { createSlsHttpProxy } from 'https://x.nest.land/sls_http_proxy@0.0.2/mod.ts'

// get tokens from env `TOKENS` and split with `,`
const tokens = (Deno.env.get('TOKENS') ?? '').split(',')

const proxy = createSlsHttpProxy({ tokens })

await serve(proxy)
```

## Send Request through proxy

for example: `fetch` send request to `https://api.example.com/v1/products`

```js
const response = await fetch('https://your-proxy-host/v1/products', {
  headers: new Headers({
    'Px-Host': 'api.example.com',
    'Px-Token': 'your-proxy-token',
  })
})
```
